Ansible MediaWiki Role
=========

This role is used to install, update and maintain MediaWiki software.

Requirements
------------

This role does not have any requirements. You might want to use other Ansible roles to setup PHP, MySQL, and SSL certificates.

Role Variables
--------------


Dependencies
------------

No dependencies

Example Playbook
----------------

Without any preparing of the server, you can simply do ;

    - hosts: servers
      roles:
         - ansible-role-mediawiki

License
-------

AGPLv3

Author Information
------------------

Created by Nono, for La Quadrature Du Net ( https://lqdn.fr )
